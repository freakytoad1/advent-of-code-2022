package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"
)

func main() {
	defer exeTime("main")()
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))
	// p1, p2 := parse(string(content))
	log.Printf("P1: %d", p1)
	log.Printf("P2: %d", p2)
}

func parse(input string) (int, int) {
	// generate 2d array from input
	treeMap := parseTo2D(input)

	// find the visible trees
	return treeMap.CountVisibleAndScenic()
}

// returns the # of trees visible from the edge of forest
// and the biggest scenic score of all trees
func (f *forest) CountVisibleAndScenic() (int, int) {
	// we know the edges are always visible so use that as starting amount
	visibleCount := len(f.trees)*2 + len(f.trees[0])*2 - 4 // - 4 for the shared corners

	// visibleCount := 0
	largestScenicScore := 0

	// look at each non-edge index for visbility and scenic score
	for i := 1; i < len(f.trees)-1; i++ {
		for j := 1; j < len(f.trees[0])-1; j++ {
			visible, scenicScore := f.totalVisibleAndScore(i, j)
			visibleCount += visible // update count

			if scenicScore > largestScenicScore {
				largestScenicScore = scenicScore
			}
		}
	}

	return visibleCount, largestScenicScore
}

func (f *forest) totalVisibleAndScore(row, col int) (int, int) {
	tree := f.trees[row][col]
	// if visible or not
	visible := 0

	// scenic score of this tree
	score := 1

	for i := 0; i < 4; i++ {
		visibleResult, scoreResult := f.directionalVisibleAndScore(tree, row, col, i, 0, true)
		if visibleResult > 0 {
			visible = visibleResult
		}

		score = scoreResult * score
	}

	return visible, score
}

func (f *forest) directionalVisibleAndScore(height, row, col, dir, score int, first bool) (int, int) {
	// compare to next tree
	// first signifies that this was the start of recursion, don't compare the tree to itself
	if !first && (height <= f.trees[row][col]) {
		return 0, score
	}

	if f.isEdge(row, col) {
		// if we reached here it was compared against an edge and was visible
		return 1, score
	}

	// increment scenic score
	score++

	switch dir {
	case 0: // up
		if row != 0 {
			return f.directionalVisibleAndScore(height, row-1, col, dir, score, false)
		}
	case 1: // down
		if row != len(f.trees)-1 {
			return f.directionalVisibleAndScore(height, row+1, col, dir, score, false)
		}
	case 2: // left
		if col != 0 {
			return f.directionalVisibleAndScore(height, row, col-1, dir, score, false)
		}
	case 3: // right
		if col != len(f.trees[0])-1 {
			return f.directionalVisibleAndScore(height, row, col+1, dir, score, false)
		}
	}

	return 0, score
}

func (f *forest) isEdge(row, col int) bool {
	return row == 0 || col == 0 || row == len(f.trees)-1 || col == len(f.trees[0])-1
}

type forest struct {
	trees [][]int
}

// parseTo2D takes the input and creates a 2d array
// the 'map' of trees
func parseTo2D(input string) *forest {
	// find the 'y' length of the 2d array needed
	splitInput := strings.Split(input, "\n")

	// init rows
	treeMap := make([][]int, len(splitInput))

	// init each column
	for i := range treeMap {
		treeMap[i] = make([]int, len(splitInput[0]))
	}

	// fill in trees with their heights
	for i, line := range splitInput {
		for j, char := range line {
			// convert to int
			height, err := strconv.Atoi(string(char))
			if err != nil {
				log.Fatalf("couldn't convert %q to #: %v", char, err)
			}

			treeMap[i][j] = height
		}
	}

	return &forest{
		trees: treeMap,
	}
}

// func to calculate and print execution time
func exeTime(name string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s execution time: %v\n", name, time.Since(start))
	}
}
