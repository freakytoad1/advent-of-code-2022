package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"
)

func main() {
	defer exeTime("main")()
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))
	// p1, p2 := parse(string(content))
	log.Printf("P1: %d", p1)
	log.Printf("P2: %d", p2)
}

type Rope struct {
	// current spot of head and tail
	// [0] = x, [1] = y
	head []int
	tail []int

	// will hold the visited spots of the tail,
	// key = "x,y" ints as a string
	visited map[string]bool
}

func newRope() *Rope {
	// starting positions
	return &Rope{
		head: []int{0, 0},
		tail: []int{0, 0},
		visited: map[string]bool{
			"00": true,
		},
	}
}

func parse(input string) (int, int) {
	r := newRope()

	// parse all the instructions coming in
	for _, line := range strings.Split(input, "\n") {
		// determine the instruction
		direction, amount := parseMove(line)

		// move the head/tail
		r.move(direction, amount)
	}

	return len(r.visited), 0
}

func parseMove(input string) (string, int) {
	// D = Down
	// U = Up
	// L = Left
	// R = Right

	splitMove := strings.Split(input, " ")
	amount, err := strconv.Atoi(splitMove[1])
	if err != nil {
		log.Fatalf("%s invalid: %v", input, err)
	}

	return splitMove[0], amount
}

func (r *Rope) move(direction string, amount int) {
	// move the head in that direction, that amount
	// move 1 space at a time
	for i := 1; i < amount+1; i++ {
		switch direction {
		case "U":
			r.head[1] = r.head[1] - 1
		case "D":
			r.head[1] = r.head[1] + 1
		case "L":
			r.head[0] = r.head[0] - 1
		case "R":
			r.head[0] = r.head[0] + 1
		}

		// move tail if not touching
		if !r.isTailTouchingHead() {
			r.moveTail()
		}
	}
}

func (r *Rope) moveTail() {
	// need to move tail to be touching heads new pos
	// and update positions visited

	// r = y [1]
	// c = x [0]

	if r.head[1] < r.tail[1] {
		if abs(r.head[1]-r.tail[1]) > abs(r.head[0]-r.tail[0]) {
			r.tail[1] = r.tail[1] - 1
			r.tail[0] = r.head[0]
		} else if r.head[0] < r.tail[0] {
			r.tail[1] = r.tail[1] - 1
			r.tail[0] = r.tail[0] - 1
		} else {
			r.tail[1] = r.tail[1] - 1
			r.tail[0] = r.tail[0] + 1
		}
	} else if r.head[1] > r.tail[1] {
		if abs(r.head[1]-r.tail[1]) > abs(r.head[0]-r.tail[0]) {
			r.tail[1] = r.tail[1] + 1
			r.tail[0] = r.head[0]
		} else if r.head[0] < r.tail[0] {
			r.tail[1] = r.tail[1] + 1
			r.tail[0] = r.tail[0] - 1
		} else {
			r.tail[1] = r.tail[1] + 1
			r.tail[0] = r.tail[0] + 1
		}
	} else if r.head[0] < r.tail[0] {
		r.tail[1] = r.head[1]
		r.tail[0] = r.head[0] - 1
	} else {
		r.tail[1] = r.head[1]
		r.tail[0] = r.tail[0] + 1
	}

	// update visited
	r.visited[r.getTailCoordinates()] = true
}

func (r *Rope) getTailCoordinates() string {
	return fmt.Sprintf("%d%d", r.tail[0], r.tail[1])
}

// Abs returns the absolute value of x.
func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func (r *Rope) isTailTouchingHead() bool {
	// on same spot
	if r.head[0] == r.tail[0] && r.head[1] == r.tail[1] {
		return true
	}

	// above or below
	if r.head[0] == r.tail[0] && ((r.tail[1] == r.head[1]-1) || (r.tail[1] == r.head[1]+1)) {
		return true
	}

	// left or right
	if r.head[1] == r.tail[1] && ((r.tail[0] == r.head[0]-1) || (r.tail[0] == r.head[0]+1)) {
		return true
	}

	// upper left or upper right
	if r.tail[1] == r.head[1]-1 && ((r.tail[0] == r.head[0]-1) || (r.tail[0] == r.head[0]+1)) {
		return true
	}

	// lower left or lower right
	if r.tail[1] == r.head[1]+1 && ((r.tail[0] == r.head[0]-1) || (r.tail[0] == r.head[0]+1)) {
		return true
	}

	return false
}

// func to calculate and print execution time
func exeTime(name string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s execution time: %v\n", name, time.Since(start))
	}
}
