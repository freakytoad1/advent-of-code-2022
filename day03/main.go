package main

import (
	"io/ioutil"
	"log"
	"strings"
	"unicode"
)

func main() {
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))

	log.Printf("Part 1: %d", p1)
	log.Printf("Part 2: %d", p2)
}

func parse(input string) (int, int) {
	p1, p2 := 0, 0
	i := 0
	grp := &group{}
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}
		if i >= 3 {
			i = 0
			grp = &group{}
		}

		// part 1
		r := newRuckSack(line)
		p1 += r.priority

		// part 2
		// every 3 rucksacks is a group,
		// find the common letter between them and total all those priorities
		grp.elves = append(grp.elves, r)

		if i == 2 {
			// time to find the common
			duplicate := findDuplicatePart2(grp.elves[0].items, grp.elves[1].items, grp.elves[2].items)
			p2 += determinePriority(duplicate)
		}

		i++
	}

	return p1, p2
}

type group struct {
	elves []*rucksack
}

type rucksack struct {
	items     string
	comp1     string
	comp2     string
	duplicate rune
	priority  int
}

func newRuckSack(items string) *rucksack {
	r := &rucksack{
		items: items,
	}
	r.split()                                   // split the items into 2 compartments
	r.findDuplicate()                           // find the common item
	r.priority = determinePriority(r.duplicate) // find the priority of the duplicated item
	return r
}

func (r *rucksack) split() {
	half := len(r.items) / 2
	r.comp1 = r.items[:half]
	r.comp2 = r.items[half:]
}

// finds the letter that is duplicated between comp1 and comp2
func (r *rucksack) findDuplicate() {
	// put 1st comp into a map for faster searching
	comp1Map := make(map[rune]struct{})
	for _, letter := range r.comp1 {
		comp1Map[letter] = struct{}{}
	}

	// iterate over comp2 and look up if there is a duplicate in comp1
	for _, letter := range r.comp2 {
		if _, found := comp1Map[letter]; found {
			r.duplicate = letter
			return
		}
	}

	log.Print("no duplicate found")
}

func stringToRuneMap(input string) map[rune]int {
	resultMap := make(map[rune]int)
	for _, letter := range input {
		if _, found := resultMap[letter]; found {
			resultMap[letter] = resultMap[letter] + 1
		} else {
			resultMap[letter] = 1
		}
	}

	return resultMap
}

func findDuplicatePart2(one, two, three string) rune {
	// make a map for each input, see what is in all three
	// key is the letter, value is the count of appearances
	oneCounts := stringToRuneMap(one)
	twoCounts := stringToRuneMap(two)

	// iterate over the third and see if the letter is found in map,
	// and if the map value is 2, this is the answer.
	for _, letter := range three {
		if _, found := oneCounts[letter]; found {
			if _, found2 := twoCounts[letter]; found2 {
				return letter
			}
		}
	}

	log.Print("unable to find matching string between 3 elves")
	return 0
}

// Lowercase item types a through z have priorities 1 through 26.
// Uppercase item types A through Z have priorities 27 through 52.
func determinePriority(input rune) int {
	// 'a' in ASCII is 97, so subtract (97-1)=96
	removal := 96

	if unicode.IsUpper(input) {
		// 'A' in ASCII is 65, so subtract (65-27) = 38 if uppercase
		removal = 38
	}
	return int(input) - removal
}
