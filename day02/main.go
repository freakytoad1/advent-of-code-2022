package main

import (
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))

	log.Printf("Part 1: %d", p1)
	log.Printf("Part 2: %d", p2)
}

func parse(input string) (int, int) {
	part1, part2 := 0, 0
	// Determine the opponent's move and your move
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}

		roundMoves := strings.Split(line, " ")
		p1pts, p2pts := points(roundMoves[0], roundMoves[1])
		part1 += p1pts
		part2 += p2pts
	}

	return part1, part2
}

// moves are ABC
// A beats C
// B beats A
// C beats B
var moves = map[string]int{
	"A": 1, // rock
	"B": 2, // paper
	"C": 3, // scissors
}

var standardize = map[string]string{
	"X": "A",
	"Y": "B",
	"Z": "C",
}

// return is your points for this 'round'
func points(opponent, you string) (int, int) {
	// winning = 6
	// draw = 3
	// losing = 0
	// rock = 1
	// paper = 2
	// scissors = 3

	part1 := moves[standardize[you]]

	// guaranteed at least 1 point per round
	part1Win, part2Win := determineWinner(opponent, you)
	part1 += part1Win
	part2 := part2Win + p2PickMove(opponent, you, part2Win)

	log.Printf("Op: %s, You: %s, P1: %d, P2: %d", opponent, you, part1, part2)

	return part1, part2
}

// returns 0 for loss, 3 for draw, 6 for win
// return 1 is for part1, return2 is part 2
// for part 2, 'you' value determines whether you win or lose that round
func determineWinner(opponent, you string) (int, int) {
	you = standardize[you]
	part1, part2 := 0, 0

	log.Printf("Op: %s, You: %s", opponent, you)

	// A (rock) beats C (scissors)
	// B (paper) beats A (rock)
	// C (scissors) beats B (paper)
	switch {
	case you == opponent: // draw
		part1 = 3
	case you == "A" && opponent == "C": // win
		part1 = 6
	case you == "B" && opponent == "A": // win
		part1 = 6
	case you == "C" && opponent == "B": // win
		part1 = 6
	}

	// part 2
	// A = lose,
	// B = draw,
	// C = win
	switch you {
	case "B": // draw
		part2 = 3
	case "C": // win
		part2 = 6
	}

	return part1, part2
}

// returns the points for part 2 move based on the expected win condition
// determines what move you should make to reach condition (and how many points)
func p2PickMove(opponent, you string, winCond int) int {
	you = standardize[you]

	switch winCond {
	case 0: // lose
		switch opponent {
		case "A":
			return moves["C"]
		case "B":
			return moves["A"]
		case "C":
			return moves["B"]
		}
	case 3: // draw
		return moves[opponent]
	case 6: // win
		switch opponent {
		case "A":
			return moves["B"]
		case "B":
			return moves["C"]
		case "C":
			return moves["A"]
		}
	}

	return 0
}
