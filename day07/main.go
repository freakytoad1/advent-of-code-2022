package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"
)

func main() {
	defer exeTime("main")()
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))
	log.Printf("P1: %d", p1)
	log.Printf("P2: %d", p2)
}

func parse(input string) (int, int) {
	t := newTree()
	currentNode := t.root
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}

		splitOut := strings.Split(line, " ")

		// determine the command
		if splitOut[0] == "$" {
			if splitOut[1] == "cd" {
				// go into a directory, assume the dir already exists
				// change 'currentNode'

				if splitOut[2] == "/" {
					continue
				}

				// go up if '..'
				if splitOut[2] == ".." {
					currentNode = currentNode.parent
				} else {
					currentNode = currentNode.findImmediate(splitOut[2])
				}
			}
		} else if splitOut[0] == "dir" {
			// name of a directory to be placed in the current directory
			addDirToParent(currentNode, splitOut[1])
		} else {
			// has to be a file
			addFileToParent(currentNode, parseFileSize(line))
		}
	}

	return t.root.GetDirectorySum(100000), t.root.deleteWhich(70000000, 30000000)
}

func parseFileSize(input string) int {
	// EX) 24836 rsjcg.lrh
	// Always a number then a filename separated by space
	splitOut := strings.Split(input, " ")
	size, err := strconv.Atoi(splitOut[0])
	if err != nil {
		log.Fatalf("couldn't convert to # [%s]: %v", input, err)
	}
	return size
}

type tree struct {
	root *node
}

type node struct {
	parent   *node
	children []*node // non-nil for dirs
	size     int     // for dirs, this is the total size of all files/dirs it contains
	name     string  // name of this dir
}

func newTree() *tree {
	return &tree{
		root: &node{name: "/"},
	}
}

// add a file to the parent node (dir) and updates the dir's and all higher dir's sizes
func addFileToParent(parent *node, fileSize int) {
	// don't need to save files to answer the problems

	// update this and all higher parent's sizes
	updateParentsSize(parent, fileSize)
}

func updateParentsSize(parent *node, size int) {
	curr := parent
	for curr != nil {
		curr.size += size
		curr = curr.parent
	}
}

func addDirToParent(parent *node, dir string) {
	parent.children = append(parent.children, &node{
		parent: parent,
		name:   dir,
	})
}

// gets all dirs sizes and sums them
// determines which is best to delete to achieve the 'neededSize'
func (n *node) deleteWhich(totalSizeAvailable, updateSize int) int {
	// answer is smallest size dir that could be removed to support an 'update'

	// size of the top level directory
	topLevelSize := n.size

	// sizes of all directories
	sizes := n.getDirectorySizes()

	spaceAvailable := totalSizeAvailable - topLevelSize
	neededSize := updateSize - spaceAvailable

	contender := topLevelSize
	for _, size := range sizes {
		if size >= neededSize && size < contender {
			contender = size
		}
	}

	return contender
}

// gets all dirs with a size of <= the maxSize
// sums them all up
func (n *node) GetDirectorySum(maxSize int) int {
	sizes := n.getDirectorySizes()

	result := 0

	for _, size := range sizes {
		if size <= maxSize {
			result += size
		}
	}

	return result
}

// traverses node and get the size of each directory
func (n *node) getDirectorySizes() []int {
	size := []int{n.size}

	for _, child := range n.children {
		size = append(size, child.getDirectorySizes()...)
	}

	return size
}

// returns the dir in this node with the requested name
func (n *node) findImmediate(dir string) *node {
	// no more dirs to search here
	if n.children == nil {
		return nil
	}

	for _, child := range n.children {
		if child.name == dir {
			return child
		}
	}

	return nil
}

// func to calculate and print execution time
func exeTime(name string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s execution time: %v\n", name, time.Since(start))
	}
}
