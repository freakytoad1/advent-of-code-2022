package main

import (
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
)

func main() {
	// ingest input file with calorie counts
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	answer := parse(string(content))

	log.Printf("answer 1: %d", answer[0])
	log.Printf("answer 2: %d+%d+%d=%d", answer[0], answer[1], answer[2], answer[0]+answer[1]+answer[2])
}

// return is a map of elf # to total cals
func parse(input string) []int {
	current := 0

	output := []int{}

	// each line is a calorie amount
	// empty lines signify a break between elves
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 || line == "" {
			// done with current elf, add to slice, reset
			output = append(output, current)
			current = 0
			continue
		}

		// convert line to int
		i, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("%q couldn't be converted to int!: %v", line, err)
		}

		current += i
	}

	// sort
	sort.Slice(output, func(p, q int) bool {
		return output[p] > output[q]
	})

	return output
}
