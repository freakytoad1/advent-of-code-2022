package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"time"
)

func main() {
	defer exeTime("main")()
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	log.Printf("P1: %d", parse(string(content), 4))
	log.Printf("P2: %d", parse(string(content), 14))
}

func parse(input string, lengthReq int) int {
	// answer is the index of the char that is not repeated in the previous 4 chars
	vals := []rune{}

	for i, letter := range input {
		vals = append(vals, letter)

		if len(vals) > lengthReq {
			// remove first
			vals = vals[1:]
		} else {
			// not full yet
			continue
		}

		// log.Print(vals)

		// look at all, if there are duplicates we must continue
		if areDuplicates(vals) {
			continue
		}

		return i + 1
	}

	return 0
}

func areDuplicates(input []rune) bool {
	prev := make(map[rune]struct{})
	for _, x := range input {
		// check if rune is already in the map
		if _, found := prev[x]; found {
			return true
		}

		// save for next iteration
		prev[x] = struct{}{}
	}

	// no duplicates
	return false
}

// func to calculate and print execution time
func exeTime(name string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s execution time: %v\n", name, time.Since(start))
	}
}
