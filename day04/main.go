package main

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))

	log.Printf("P1: %d", p1)
	log.Printf("P2: %d", p2)
}

func parse(input string) (int, int) {
	p1 := 0
	p2 := 0
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}

		pairedElves := parsePair(line)
		if pairedElves.fullyContained() {
			p1++
		}
		if pairedElves.overlap() {
			p2++
		}
	}

	return p1, p2
}

type pair struct {
	elf1 *sections
	elf2 *sections
}

type sections struct {
	start string
	end   string
}

func parsePair(input string) *pair {
	// will look like '1-2,6-9'
	// elf 1 has first part, elf 2 has 2nd
	elves := strings.Split(input, ",")

	return &pair{
		elf1: parseSections(elves[0]),
		elf2: parseSections(elves[1]),
	}
}

func parseSections(input string) *sections {
	// will look like '1-2'
	splitSections := strings.Split(input, "-")
	return &sections{
		start: splitSections[0],
		end:   splitSections[1],
	}
}

func (p *pair) fullyContained() bool {
	// returns true if one of the elves sections is fully in the other.
	// checks 1 in 2, and 2 in 1
	if p.elf1.contains(p.elf2) || p.elf2.contains(p.elf1) {
		return true
	}

	return false
}

func (s *sections) contains(v *sections) bool {
	// returns true if 'v' is fully contained within 's'
	// we can assume the aoc input is clean
	sStart, _ := strconv.Atoi(s.start)
	sEnd, _ := strconv.Atoi(s.end)

	vStart, _ := strconv.Atoi(v.start)
	vEnd, _ := strconv.Atoi(v.end)

	// EX) s = 2-7, v = 3-4
	// EX) s = 2-3, v = 3-3
	if vStart >= sStart && vEnd <= sEnd {
		// contained
		return true
	}

	return false
}

func (p *pair) overlap() bool {
	// returns true if the elves' sections overlap at all
	return p.elf1.overlaps(p.elf2)
}

func (s *sections) overlaps(v *sections) bool {
	// returns true if 'v' has any sections within 's'
	// we can assume the aoc input is clean
	sStart, _ := strconv.Atoi(s.start)
	sEnd, _ := strconv.Atoi(s.end)

	vStart, _ := strconv.Atoi(v.start)
	vEnd, _ := strconv.Atoi(v.end)

	sMap := rangetoMap(sStart, sEnd)

	for i := vStart; i <= vEnd; i++ {
		if _, found := sMap[i]; found {
			return true
		}
	}

	return false
}

func rangetoMap(start, end int) map[int]struct{} {
	result := make(map[int]struct{})
	for i := start; i <= end; i++ {
		result[i] = struct{}{}
	}

	return result
}
