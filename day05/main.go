package main

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	// ingest input file
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))

	log.Printf("P1: %s", p1)
	log.Printf("P2: %s", p2)
}

func parse(input string) (string, string) {
	stackP1 := parseStacks(input)
	stackP2 := parseStacks(input)

	// parse the moves
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}

		currentMove := parseMove(line)
		// part 1 moves crates 1 by 1
		stackP1.moveCratesP1(currentMove)

		// part 2 moves crates all at once
		stackP2.moveCratesP2(currentMove)
	}

	return stackP1.topLevels(), stackP2.topLevels()
}

func (s *Stacks) moveCratesP1(moves *move) {
	// EX) move (# of crates) from (stack #) to (stack #)
	for i := 0; i < moves.amount; i++ {
		// pop the 'from', push that to 'to'
		toMove := s.Stacks[moves.from-1].Pop()
		s.Stacks[moves.to-1].Push(toMove)
	}
}

func (s *Stacks) moveCratesP2(moves *move) {
	// Need to move all toMove, retaining the order
	// gather the toMove from 'from'
	toMove := []string{}
	for i := 0; i < moves.amount; i++ {
		// this should insert at the front of the slice, maintaining order
		toMove = append([]string{s.Stacks[moves.from-1].Pop()}, toMove...)
	}

	log.Printf("amount: %d, from: %d, to: %d; crates: %v", moves.amount, moves.from, moves.to, toMove)

	// add the crates to 'to'
	for _, crate := range toMove {
		s.Stacks[moves.to-1].Push(crate)
	}
}

func (s *Stacks) topLevels() string {
	result := ""
	for _, st := range s.Stacks {
		result += st.Peak()
	}

	return result
}

type move struct {
	amount int
	from   int
	to     int
}

func parseMove(input string) *move {
	// EX) move (# of crates) from (stack #) to (stack #)
	result := []int{}
	for _, word := range strings.Split(input, " ") {
		anInt, err := strconv.Atoi(word)
		if err != nil {
			// not an int
			continue
		}

		// save the ints
		result = append(result, anInt)
	}

	if len(result) != 3 {
		log.Fatalf("%s, wrong amount of #s", input)
	}

	return &move{
		amount: result[0],
		from:   result[1],
		to:     result[2],
	}
}

// [D]                     [N] [F]
// [H] [F]             [L] [J] [H]
// [R] [H]             [F] [V] [G] [H]
// [Z] [Q]         [Z] [W] [L] [J] [B]
// [S] [W] [H]     [B] [H] [D] [C] [M]
// [P] [R] [S] [G] [J] [J] [W] [Z] [V]
// [W] [B] [V] [F] [G] [T] [T] [T] [P]
// [Q] [V] [C] [H] [P] [Q] [Z] [D] [W]
//  1   2   3   4   5   6   7   8   9
func parseStacks(input string) *Stacks {
	// just hard code this for now
	items := []string{}
	stacks := &Stacks{}

	items = append(items, "QWPSZRHD")
	items = append(items, "VBRWQHF")
	items = append(items, "CVSH")
	items = append(items, "HFG")
	items = append(items, "PGJBZ")
	items = append(items, "QTJHWFL")
	items = append(items, "ZTWDLVJN")
	items = append(items, "DTZCJGHF")
	items = append(items, "WPVMBH")

	for _, item := range items {
		s := &Stack{}
		for _, letter := range item {
			s.Push(string(letter))
		}

		stacks.Stacks = append(stacks.Stacks, s)
	}

	return stacks
}

// Stack stuff below

type Stacks struct {
	Stacks []*Stack
}

type Stack struct {
	crates []string
}

// Push adds a new item to the top of the stack
func (c *Stack) Push(s string) {
	c.crates = append(c.crates, s)
}

// Pop removes the last item from the top of the stack and returns it
func (s *Stack) Pop() string {
	leng := len(s.crates) - 1
	if leng < 0 {
		// avoid panic
		log.Print("Cannot pop")
		return ""
	}

	toPop := s.crates[leng]
	s.crates = s.crates[:leng]
	return toPop
}

// shows the top item of stack without removing it
func (s *Stack) Peak() string {
	leng := len(s.crates) - 1
	if leng < 0 {
		// avoid panic
		return ""
	}

	return s.crates[leng]
}

func (s *Stack) Length() int {
	return len(s.crates)
}
